import { Component, OnInit } from '@angular/core';
import { ApiClientService} from '../../core/api-client.service';
import { LibvirtDomainModel } from 'src/app/interfaces/libvirt-domain.interface';

@Component({
  selector: 'app-domains-list',
  templateUrl: './domains-list.component.html',
  styleUrls: ['./domains-list.component.scss']
})
export class DomainsListComponent implements OnInit {

  data: LibvirtDomainModel[];

  constructor(private ApiClient: ApiClientService) {}

  ngOnInit() {
    this.ApiClient.getDomains().subscribe(data => this.data = data);
  }

}
