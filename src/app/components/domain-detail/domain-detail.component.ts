import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ApiClientService } from '../../core/api-client.service';

@Component({
  selector: 'app-domain-detail',
  templateUrl: './domain-detail.component.html',
  styleUrls: ['./domain-detail.component.scss']
})
export class DomainDetailComponent implements OnInit, OnDestroy {
  domainUuid: any;
  domain: any;
  quantity: number;
  private sub: any;

  constructor(
    private ApiClient: ApiClientService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.domainUuid = params.uuid;
    });
    this.ApiClient.getDomainByUuid(this.domainUuid).subscribe(data => this.domain = data);
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
