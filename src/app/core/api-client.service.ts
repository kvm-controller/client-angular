import { Injectable } from '@angular/core';
import { HttpClient  } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LibvirtDomainModel } from '../interfaces/libvirt-domain.interface';

@Injectable({
  providedIn: 'root'
})
export class ApiClientService {
  base_url = 'http://192.168.33.11:3000';

  constructor(private http: HttpClient) {}

  getDomains(): Observable<LibvirtDomainModel[]> {
    return this.http.get<LibvirtDomainModel[]>(`${this.base_url}/domains`);
  }

  getDomainByUuid(domainUuid: string): Observable<LibvirtDomainModel[]> {
    return this.http.get<LibvirtDomainModel[]>(`${this.base_url}/domain/${domainUuid}`);
  }

  getHostCapabilities() {
    return this.http.get<Array<any>>(`${this.base_url}/host/capabilities`);
  }
}
