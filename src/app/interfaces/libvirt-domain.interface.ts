export interface LibvirtDomain {
  id?: number;
  name: string;
  uuid: string;  // uuid
  osType: string;  // enum?
  isActive: boolean;
  isPersistent: boolean;
}

export interface LibvirtDomainModel {
  domains: LibvirtDomain[];
  quantity: number;
}

