import { TestBed } from '@angular/core/testing';

import { HostCapabilitiesService } from './host-capabilities.service';

describe('HostCapabilitiesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HostCapabilitiesService = TestBed.get(HostCapabilitiesService);
    expect(service).toBeTruthy();
  });
});
