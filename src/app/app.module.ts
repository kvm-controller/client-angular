import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { NgMathPipesModule } from 'angular-pipes';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

import { DomainsListComponent } from './components/domains-list/domains-list.component';
import { DomainDetailComponent } from './components/domain-detail/domain-detail.component';
import { NavigationComponent } from './components/navigation/navigation.component';

@NgModule({
  declarations: [
    AppComponent,
    DomainsListComponent,
    DomainDetailComponent,
    NavigationComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgMathPipesModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
