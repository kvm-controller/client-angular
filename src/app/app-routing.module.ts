import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DomainsListComponent } from './components/domains-list/domains-list.component';
import { DomainDetailComponent } from './components/domain-detail/domain-detail.component';

const routes: Routes = [
  { path: 'domains', component: DomainsListComponent },
  { path: 'domain/:uuid', component: DomainDetailComponent },
  // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
